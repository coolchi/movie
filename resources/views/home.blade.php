@extends('layouts.app')

@section('content')
<div class="container">
    @can("isAdmin")
        <div class="row justify-content-center  ">
            <div class="col-4 p-3" style="background: white">
                <h4>Current Month Sales</h4>
                <h2>{{ $current_month }}</h2>
            </div>
            <div class="col-4 p-3" style="background: white">
                <h4>Last Month Sales</h4>
                <h2>{{ $last_month }}</h2>
            </div>

            <div class="col-4 p-4" style="background: white" >
                <h4>Total Purchase</h4>
                <h2>{{ $total_purchase }}</h2>
            </div>
        </div>
    @endcan
    <div class="row mt-5">

        @foreach ($films as $film )
        <div class="col-sm-3">
            <div class="card" >
                <img src="/images/{{ $film->image }}" class="card-img-top" style="height: 300px;" alt="{{ $film->title }}">
                <div class="card-body">
                <form method="POST" action="{{ route('purchase.store') }}">
                    @csrf
                <h5 class="card-title">{{ $film->title }}</h5>
                <p class="card-text">{{ $film->description }}</p>
                <input type="hidden" name="movie_id" value="{{ $film->id }}" />
                <input type="hidden" name="title" value="{{ $film->title }}" />
                <input type="submit" class="btn btn-primary" value="Purchase">
                </form>
                </div>
            </div>
        </div>
        @endforeach

    </div>
</div>
@endsection
