@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <img src="/images/{{ $film->image }}" class="card-img-top" style="height: 600px;" alt="{{ $film->title }}">
        </div>
        <div class="col-md-6">

            <h1>{{ $film->title }}</h1>
            <p>{{ $film->description }}</p>
            <h3>Price: {{ $film->cost }}</h3>
            @auth
            <form method="POST" action="{{ route('purchase.store') }}">
                @csrf
            <div class="row mt-5">
                <div class="col-xs-12 col-md-12 mt-5">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                Payment Details
                            </h3>
                        </div>
                        <div class="panel-body">
                            <form role="form">
                            <div class="form-group">
                                <label for="cardNumber">
                                    CARD NUMBER</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="cardNumber" placeholder="Valid Card Number"
                                        required autofocus />
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-7 col-md-7">
                                    <div class="form-group">
                                        <label for="expityMonth">
                                            EXPIRY DATE</label>
                                        <div class="col-xs-6 col-lg-6 pl-ziro">
                                            <input type="text" class="form-control" id="expityMonth" placeholder="MM" required />
                                        </div>
                                        <div class="col-xs-6 col-lg-6 pl-ziro">
                                            <input type="text" class="form-control" id="expityYear" placeholder="YY" required /></div>
                                    </div>
                                </div>
                                <div class="col-xs-5 col-md-5 pull-right">
                                    <div class="form-group">
                                        <label for="cvCode">
                                            CV CODE</label>
                                        <input type="password" class="form-control" id="cvCode" placeholder="CV" required />
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                    <br/>
                    <input type="hidden" name="movie_id" value="{{ $film->id }}" />
                    <input type="hidden" name="title" value="{{ $film->title }}" />
                    <input type="submit" class="btn btn-success btn-lg btn-block" value="Purchase">

                </div>
            </div>
        </form>

            @else
            <h2>Please Login into your account or create a new one <a href="/login" class="btn btn-primary"> Login </a> </h2>
            @endauth
        </div>
    </div>
</div>
@endsection
